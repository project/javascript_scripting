## INTRODUCTION

The Javascript Scripting module is a DESCRIBE_THE_MODULE_HERE.

The primary use case for this module is:

- Use case #1
- Use case #2
- Use case #3

## REQUIREMENTS

This module require the library js4php5, from hiltonjanfield/js4php5, but from
the fork https://github.com/PSF1/js4php5.git.

## INSTALLATION

In the project composer.json we must include the repository:
```json
"repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/PSF1/js4php5.git"
    }
  ],
```

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

Example: `composer require 'drupal/javascript_scripting'`

After enable the module in Drupal, you can try it with drush:

With depuration messages enabled:
`drush javascript:execute ../web/modules/contrib/javascript_scripting/example/test.js`

Without depuration messages enabled:
`drush javascript:execute --mute ../web/modules/contrib/javascript_scripting/example/test.js`

Note that the path to the test js file can vary.

## CONFIGURATION
- Configuration step #1
- Configuration step #2
- Configuration step #3

## MAINTAINERS

Current maintainers for Drupal 10:

- Pedro Pelaez (PSF_) - https://www.drupal.org/u/psf_
