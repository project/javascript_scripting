// Test javascript operators:
// @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Greater_than_or_equal
console.log('5 >= 3 => true', 5 >= 3);
console.log('3 >= 3 => true', 3 >= 3);
console.log(' 3 >= 5 => false', 3 >= 5);
console.log("'ab' >= 'aa' => true", 'ab' >= 'aa');

console.log('5 > 3 => true', 5 > 3);
console.log('3 > 3 => false', 3 > 3);
console.log('3 > 5 => false', 3 > 5);
console.log("'ab' > 'aa' => true", 'ab' > 'aa');

console.log("1 != 1 => false", 1 != 1);
console.log("'hello' != 'hello' => false", 'hello' != 'hello');
console.log("'1' != 1 => false", '1' != 1);
console.log("0 != false => false", 0 != false);

console.log("1 == 1 => true", 1 == 1);
console.log("'hello' == 'hello' => true", 'hello' == 'hello');
console.log("'1' == 1 => true", '1' == 1);
console.log("0 == false => true", 0 == false);

console.log("5 < 3 => false", 5 < 3);
console.log("3 < 3 => false", 3 < 3);
console.log("3 < 5 => true", 3 < 5);
console.log("'aa' < 'ab' => true", 'aa' < 'ab');

console.log("5 <= 3 => false", 5 <= 3);
console.log("3 <= 3 => true", 3 <= 3);
console.log("3 <= 5 => true", 3 <= 5);
console.log("'aa' <= 'ab' => true", 'aa' <= 'ab');

console.log("1 === 1 => true", 1 === 1);
console.log("'hello' === 'hello' => true", 'hello' === 'hello');
console.log("'1' === 1 => false", '1' === 1);
console.log("0 === false => false", 0 === false);

console.log("1 !== 1 => false", 1 !== 1);
console.log("'hello' !== 'hello' => false", 'hello' !== 'hello');
console.log("'1' !== 1 => true", '1' !== 1);
console.log("0 !== false => true", 0 !== false);

// Suponiendo que tenemos A1394 y RESUMEN_J8 como variables en JavaScript
var A1394 = 900; // Valor de la celda A1394
var RESUMEN_J8 = 12; // Valor de RESUMEN.J8
var resultado = 0;

// Asegúrate de que RESUMEN_J8 no sea cero para evitar error de división por cero
if(RESUMEN_J8 !== 0) {
  resultado = A1394 / RESUMEN_J8;
  if (resultado > 1000) {
    // Redondear hacia abajo al múltiplo de 10 más cercano
    resultado = Math.floor(resultado / 10) * 10;
  } else if (resultado > 100) {
    // Redondear al número entero más cercano
    resultado = Math.round(resultado);
  } else {
    // Redondear al número entero más cercano
    resultado = Math.round(resultado);
  }
} else {
  // Manejar la división por cero si es necesario
  // Establecer resultado a algún valor de error o manejarlo como sea necesario en tu aplicación
}

Console.log('resultado', resultado);

/**
 * Array or Object to string.
 *
 * @see https://stackoverflow.com/a/3011557/6385708
 *
 * @param arr Object to dump.
 * @param level Internal use only.
 *
 * @returns {string}
 */
function mydump(arr,level) {
  var dumped_text = "";
  if(!level) level = 0;

  var level_padding = "";
  for(var j=0;j<level+1;j++) level_padding += "    ";

  if(typeof arr == 'object') {
    for(var item in arr) {
      var value = arr[item];

      if(typeof value == 'object') {
        dumped_text += level_padding + "'" + item + "' ...\n";
        dumped_text += mydump(value,level+1);
      } else {
        dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
      }
    }
  } else {
    dumped_text = "===>"+arr+"<===("+(typeof arr)+")";
  }
  return dumped_text;
}

var cosa = {
  nombre: "Pedro",
  apellidos: "Apellidos"
};

Console.log(mydump(cosa));

return cosa;
