<?php

namespace Drupal\javascript_scripting\Drush\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\javascript_scripting\JsContext\JsConsole;
use Drupal\javascript_scripting\JsRunnerServiceInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use js4php5\compiler\parser\ParseException;
use js4php5\JS;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush commandfile.
 */
final class JavascriptScriptingCommands extends DrushCommands {

  /**
   * Constructs JavascriptScriptingCommands object.
   */
  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly JsRunnerServiceInterface $jsRunner,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('javascript_scripting.runner'),
    );
  }

  /**
   * Command description here.
   */
  #[CLI\Command(name: 'javascript:execute', aliases: ['js'])]
  #[CLI\Argument(name: 'file', description: 'Javascript file to execute.')]
  #[CLI\Option(name: 'mute', description: 'Mute message from script.')]
  #[CLI\Usage(name: 'javascript:execute example.js', description: 'Execute example.js code.')]
  public function javascriptExecute($file, $options = ['mute' => FALSE]) {
    $resp = '';
    try {
      $script = file_get_contents($file);
      if (!is_dir('../tmp')) {
        mkdir('../tmp');
      }
      JS::setCacheDir('../tmp');
      JsConsole::setIo($this->io());
      $resp = $this->jsRunner->jsExec($script, !$options['mute']);
    }
    catch (ParseException $e) {
      /** @var \js4php5\compiler\lexer\Token $token */
      $token = $e->getToken();
      $this->io()->error(sprintf(
        '%s, "%s", in line %s, col %s',
        $token->getType(),
        $token->getText(),
        $token->getStart()->getLine(),
        $token->getStart()->getCol()
      ));
    }

    $this->io()->info('Script return: ' . print_r($resp, TRUE));

    $this->logger()->success(dt('Achievement unlocked.'));
    return Command::SUCCESS;
  }

}
