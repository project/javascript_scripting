<?php

declare(strict_types=1);

namespace Drupal\javascript_scripting;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\javascript_scripting\JsContext\JsConsole;
use js4php5\JS;
use js4php5\runtime\Runtime;

/**
 * Javascript runner service.
 */
class JsRunnerService implements JsRunnerServiceInterface {

  /**
   * Start extenders.
   *
   * @var callable[]
   */
  protected $extenders = [];

  /**
   * Constructs a JsRunnerService object.
   */
  public function __construct(
    protected readonly EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function jsExec(string $code, bool $noMute = TRUE, callable $extender = NULL, bool $forceRecompile = FALSE, bool $cacheToFile = TRUE): mixed {
    if (!is_dir('../tmp')) {
      mkdir('../tmp');
    }
    JS::setCacheDir('../tmp');
    if ($noMute) {
      $this->extenders[] = [self::class, 'jsStartExtenderWithConsole'];
    }
    else {
      $this->extenders[] = [self::class, 'jsStartExtenderWithoutConsole'];
    }
    if ($extender) {
      $this->extenders[] = $extender;
    }
    Runtime::setStartExtender([$this, 'jsStartExtender']);
    $resp = JS::run($code, NULL, $forceRecompile, $cacheToFile);

    return $resp;
  }

  /**
   * At start runtime, execute all defined extenders.
   */
  public function jsStartExtender() {
    foreach ($this->extenders as $extender) {
      call_user_func($extender);
    }
  }

  /**
   * Extend runtime context after start it.
   */
  public static function jsStartExtenderWithConsole() {
    $console = new JsConsole();
    JsConsole::useHtml(TRUE);
    Runtime::define_variable("Console", $console);
    Runtime::define_variable("console", $console);
    Runtime::push_context($console);
    Runtime::define_function([JsConsole::class, 'log'], "log");
    Runtime::pop_context();
  }

  /**
   * Extend runtime context after start it.
   */
  public static function jsStartExtenderWithoutConsole() {
    $console = new JsConsole();
    Runtime::define_variable("Console", $console);
    Runtime::define_variable("console", $console);
    Runtime::push_context($console);
    Runtime::define_function([JsConsole::class, 'blackHole'], "log");
    Runtime::pop_context();
  }

}
