<?php

declare(strict_types=1);

namespace Drupal\javascript_scripting\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\javascript_scripting\JsRunnerService;
use js4php5\compiler\parser\ParseException;
use js4php5\runtime\jsException;
use js4php5\runtime\Runtime;

/**
 * Defines the 'javascript_scripting_script_field' field type.
 *
 * @FieldType(
 *   id = "javascript_scripting_script_field",
 *   label = @Translation("Script"),
 *   description = @Translation("Isolated javascript code."),
 *   default_widget = "javascript_scripting_script_widget",
 *   default_formatter = "basic_string",
 *   cardinality = 1,
 * )
 */
class ScriptFieldItem extends FieldItemBase {

  /**
   * Parameters and values that we hace to set before javascript execution.
   *
   * @var array
   */
  protected $preJsExecutionParameters = [];

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings(): array {
    $settings = [];
    return $settings + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings(): array {
    $settings = ['muted' => TRUE];
    return $settings + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    $element['muted'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Mute script messages'),
      '#default_value' => $this->getSetting('muted'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return match ($this->get('value')->getValue()) {
      NULL, '' => TRUE,
      default => FALSE,
    };
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {

    // @DCG
    // See /core/lib/Drupal/Core/TypedData/Plugin/DataType directory for
    // available data types.
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Script'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {

    $columns = [
      'value' => [
        'type' => 'text',
        'not null' => FALSE,
        'description' => 'Script code.',
        'size' => 'big',
      ],
    ];

    $schema = [
      'columns' => $columns,
      // @todo Add indexes here if necessary.
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $random = new Random();
    $values['value'] = sprintf('return "Hi %s !";', $random->word(mt_rand(1, 50)));
    return $values;
  }

  /**
   * Execute the field script.
   *
   * @param array $parameters
   *   Parameters to inject.
   *
   * @return mixed
   *   Script result.
   */
  public function executionResult(array $parameters = []): mixed {
    try {
      ob_start();
      // This is required here to clean parameters before execution.
      $this->preJsExecutionParameters = $parameters;
      $script = $this->values['value'] ?? '';
      if (empty($script)) {
        $fieldDefinition = $this->getFieldDefinition();
        $script = $fieldDefinition->getDefaultValueLiteral()[0]["value"];
      }
      $result = self::jsRunnerService()->jsExec($script, TRUE, [$this, 'loadJavascriptParameters']);
      ob_get_clean();
      return $result;
    }
    catch (jsException $e) {
      $console = ob_get_clean();
      $slots = $e->value->slots;
      /** @var \js4php5\runtime\jsAttribute $slot */
      foreach ($slots as $slot) {
        /** @var \js4php5\runtime\Base $value */
        $value = $slot->value;
        $console .= '<p>' . $value->value . '</p>';
        \Drupal::logger('javascript_scripting')->error($console);
      }
    }
    catch (ParseException $e) {
      $console = ob_get_clean();
      /** @var \js4php5\compiler\lexer\Token $token */
      $token = $e->getToken();
      $console .= '<p>' . sprintf(
          '%s, "%s", in line %s, col %s',
          $token->getType(),
          $token->getText(),
          $token->getStart()->getLine(),
          $token->getStart()->getCol()
        ) . '</p>';
      \Drupal::logger('javascript_scripting')->error($console);
    }

    return NULL;
  }

  /**
   * Callback to add parameters to javascript runtime.
   */
  public function loadJavascriptParameters() {
    // Get widget defined parameters.
    $entity = $this->getEntity();
    $field_definition = $this->getFieldDefinition();
    $field_name = $field_definition->getName();

    $form_display = self::entityTypeManager()
      ->getStorage('entity_form_display')
      ->load($entity->getEntityTypeId() . '.' . $entity->bundle() . '.default');

    if ($form_display) {
      $widgetSettings = $form_display->getComponent($field_name)['settings'];
    }
    else {
      return;
    }

    $parameterFields = $widgetSettings["parameters"];
    // $parameterFields = $this->cleanParameters($parameterFields);
    foreach ($parameterFields as $parameterField) {
      if (isset($parameterField["type"]) && 'none' !== $parameterField["type"] && isset($parameterField["name"])) {
        $name = $parameterField["name"];
        switch ($parameterField["type"]) {
          case 'number':
            Runtime::define_variable($name, Runtime::js_int(floatval($this->preJsExecutionParameters[$name])));
            break;

          case 'string':
            Runtime::define_variable($name, Runtime::js_str($this->preJsExecutionParameters[$name]));
            break;

          case 'bool':
            Runtime::define_variable($name, Runtime::js_bool($this->preJsExecutionParameters[$name]));
            break;

          // Case 'object':
          // Runtime::define_variable(
          // $name,
          // Runtime::js_obj($this->preJsExecutionParameters[$name])
          // );
          // break;.
        }
      }
    }
  }

  /**
   * Get Javascript runner service.
   *
   * @return \Drupal\javascript_scripting\JsRunnerService
   *   Javascript runner service.
   */
  protected static function jsRunnerService(): JsRunnerService {
    return \Drupal::service('javascript_scripting.runner');
  }

  /**
   * Get the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  protected static function entityTypeManager(): EntityTypeManagerInterface {
    return \Drupal::entityTypeManager();
  }

}
