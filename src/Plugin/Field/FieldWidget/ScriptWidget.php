<?php

declare(strict_types=1);

namespace Drupal\javascript_scripting\Plugin\Field\FieldWidget;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\javascript_scripting\JsRunnerServiceInterface;
use Drupal\multivalue_form_element\Element\MultiValue;
use js4php5\compiler\parser\ParseException;
use js4php5\runtime\jsException;
use js4php5\runtime\Runtime;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the 'javascript_scripting_script_widget' field widget.
 *
 * @FieldWidget(
 *   id = "javascript_scripting_script_widget",
 *   label = @Translation("Script widget"),
 *   field_types = {"javascript_scripting_script_field"},
 * )
 */
class ScriptWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * Parameters and values that we hace to set before javascript execution.
   *
   * @var array
   */
  protected $preJsExecutionParameters = [];

  /**
   * Constructs the plugin instance.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    protected readonly JsRunnerServiceInterface $javascriptScriptingRunner,
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('javascript_scripting.runner'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $setting = ['parameters' => ''];
    return $setting + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $parameters = $this->getScriptParameters();
    $parameters = $this->cleanParameters($parameters);
    $element['parameters'] = [
      '#type' => 'multivalue',
      '#title' => $this->t('Contacts'),
      '#cardinality' => MultiValue::CARDINALITY_UNLIMITED,
      '#default_value' => $parameters,
      'name' => [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
      ],
      'type' => [
        '#type' => 'select',
        '#title' => $this->t('Type'),
        '#options' => [
          'none' => $this->t('Not used'),
          'string' => $this->t('String'),
          'number' => $this->t('Number'),
          'bool' => $this->t('Boolean'),
          // 'object' => $this->t('Object'),
        ],
      ],
      'optional' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Optional'),
      ],
      'default' => [
        '#type' => 'textfield',
        '#title' => $this->t('Default value'),
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $parameters = $this->getScriptParameters();
    $parameters = $this->cleanParameters($parameters);
    if (empty($parameters)) {
      return [''];
    }

    $rows = [$this->t('Parameters:')];
    foreach ($parameters as $parameter) {
      $rows[] = $this->t('{@type} @name @optional', [
        '@type' => $parameter['type'],
        '@name' => $parameter['name'],
        '@optional' => 0 === $parameter['optional'] ? '' : '(Default "' . $parameter["default"] . '")',
      ]);
    }
    return $rows;
  }

  /**
   * Remove not used parameters.
   *
   * @param array $parameters
   *   Parameters.
   *
   * @return array
   *   Clean parameters.
   */
  protected function cleanParameters(array &$parameters): array {
    $indexToIgnore = [];
    foreach ($parameters as $index => $parameter) {
      if ('none' === $parameter['type']) {
        $indexToIgnore[] = $index;
      }
    }
    foreach ($indexToIgnore as $index) {
      unset($parameters[$index]);
    }

    return $parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $parameters = $this->getScriptParameters();
    $parameters = $this->cleanParameters($parameters);
    $element['value'] = $element + [
      '#type' => 'textarea',
      '#default_value' => $items[$delta]->value ?? NULL,
    ];

    $element['dev_area'] = [
      '#type' => 'details',
      '#title' => $this->t('Test area'),
      '#description' => $this->t('ECMAscript/JavaScript interpreter, as defined by Ecma-262 3rd edition. We can use console.log() to debug. The final response must be returned with "return value;",'),
    ];
    $element['dev_area']['parameters'] = [
      '#type' => 'details',
      '#title' => $this->t('Parameters'),
      '#open' => TRUE,
    ];
    foreach ($parameters as $parameter) {
      $element['dev_area']['parameters'][$items->getName() . '-' . $parameter["name"]] = [
        '#type' => 'textfield',
        '#title' => $parameter["name"] . (0 === $parameter["optional"] ? '' : ' (Default "' . $parameter["default"] . '")'),
        '#default_value' => $parameter["default"],
      ];
    }
    $element['dev_area']['dev_run'] = [
      '#type' => 'submit',
      '#value' => $this->t('Try'),
      '#ajax' => [
        'callback' => [$this, 'executeScript'],
        'event' => 'click',
        'wrapper' => 'dynamic-element-wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
        'field_name' => $items->getName(),
      ],
    ];
    $element['dev_area']['dev_run_console'] = [
      '#type' => 'item',
      '#prefix' => '<div id="' . $items->getName() . '-dev-run-console" class="' . $items->getName() . '-dev-run-console">',
      '#suffix' => '</div>',
    ];
    return $element;
  }

  /**
   * Execute the script.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Response.
   */
  public function executeScript(array $form, FormStateInterface $form_state): AjaxResponse {
    $triggeringElement = $form_state->getTriggeringElement();
    $fieldName = $triggeringElement["#ajax"]["field_name"];
    $values = $form_state->getValue($fieldName);
    $parameters = $values[0]["dev_area"]["parameters"];
    $replaceKeys = [];
    foreach ($parameters as $name => $parameter) {
      $replaceKeys[$name] = $name;
    }
    foreach ($replaceKeys as $replaceKey) {
      $subName = explode('-', $replaceKey);
      $parameters[$subName[1]] = $parameters[$replaceKey];
      unset($parameters[$replaceKey]);
    }
    try {
      ob_start();
      $this->preJsExecutionParameters = $parameters;
      $result = $this->javascriptScriptingRunner->jsExec($values[0]["value"], TRUE, [$this, 'loadJavascriptParameters']);
      $console = ob_get_clean();
      $console .= '<p>' . sprintf('Script response: %s', $result) . '</p>';
    }
    catch (jsException $e) {
      $console = ob_get_clean();
      $slots = $e->value->slots;
      /** @var \js4php5\runtime\jsAttribute $slot */
      foreach ($slots as $slot) {
        /** @var \js4php5\runtime\Base $value */
        $value = $slot->value;
        $console .= '<p>' . $value->value . '</p>';
      }
    }
    catch (ParseException $e) {
      $console = ob_get_clean();
      /** @var \js4php5\compiler\lexer\Token $token */
      $token = $e->getToken();
      $console .= '<p>' . sprintf(
        '%s, "%s", in line %s, col %s',
        $token->getType(),
        $token->getText(),
        $token->getStart()->getLine(),
        $token->getStart()->getCol()
      ) . '</p>';
    }
    // Send response.
    $response = new AjaxResponse();
    $id = explode('--', $form[$fieldName]["widget"][0]["dev_area"]["dev_run_console"]["#id"]);
    $response->addCommand(new HtmlCommand('#' . $id[0], $console));
    return $response;
  }

  /**
   * Callback to add parameters to javascript runtime.
   */
  public function loadJavascriptParameters() {
    $parameterFields = $this->getScriptParameters();
    $parameterFields = $this->cleanParameters($parameterFields);
    foreach ($parameterFields as $parameterField) {
      if (isset($parameterField["name"])) {
        $name = $parameterField["name"];
        switch ($parameterField["type"]) {
          case 'number':
            Runtime::define_variable($name, Runtime::js_int(floatval($this->preJsExecutionParameters[$name])));
            break;

          case 'string':
            Runtime::define_variable($name, Runtime::js_str($this->preJsExecutionParameters[$name]));
            break;

          case 'bool':
            Runtime::define_variable($name, Runtime::js_bool($this->preJsExecutionParameters[$name]));
            break;

          // Case 'object':
          // Runtime::define_variable(
          // $name,
          // Runtime::js_obj($this->preJsExecutionParameters[$name])
          // );
          // break;.
        }
      }
    }
  }

  /**
   * Get script parameters description.
   *
   * @return array
   *   Parameters array.
   */
  protected function getScriptParameters() {
    $parameters = $this->getSetting('parameters');
    if (empty($parameters)) {
      return [];
    }

    return $parameters;
  }

}
