<?php

declare(strict_types=1);

namespace Drupal\javascript_scripting;

/**
 * Javascript runner service.
 */
interface JsRunnerServiceInterface {

  /**
   * Execute script.
   *
   * @param string $code
   *   Javascript code to execute.
   * @param bool $noMute
   *   Allow display messages from script.
   * @param callable|null $extender
   *   Add runtime extender callback.
   * @param bool $forceRecompile
   *   Compile Javascript to PHP in each execution.
   * @param bool $cacheToFile
   *   Cache compilation result code to file.
   *
   * @return mixed
   *   Javascript execution result.
   */
  public function jsExec(string $code, bool $noMute = TRUE, callable $extender = NULL, bool $forceRecompile = FALSE, bool $cacheToFile = TRUE): mixed;

}
