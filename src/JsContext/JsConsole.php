<?php

namespace Drupal\javascript_scripting\JsContext;

use js4php5\runtime\jsObject;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Javascript console object.
 */
class JsConsole extends jsObject {

  /**
   * In drush context, the Symfony Style object.
   *
   * @var \Symfony\Component\Console\Style\SymfonyStyle|null
   */
  protected static ?SymfonyStyle $io = NULL;

  /**
   * Must console messages use HTML notation?
   *
   * @var bool
   */
  protected static bool $useHtml = FALSE;

  /**
   * Set Drush output manager.
   *
   * @param \Symfony\Component\Console\Style\SymfonyStyle $io
   *   Output manager.
   */
  public static function setIo(SymfonyStyle $io) {
    self::$io = $io;
  }

  /**
   * Must console messages use HTML notation?
   */
  public static function useHtml(bool $useHtml = TRUE) {
    self::$useHtml = $useHtml;
  }

  /**
   * Log to standard output.
   */
  public static function log() {
    $args = func_get_args();
    $values = [];
    foreach ($args as $arg) {
      $s = $arg->toStr();
      $values[] = $s->value;
    }
    $log = sprintf("%s\n", implode(', ', $values));
    if (self::$io) {
      self::$io->writeln('<fg=yellow>' . $log . '</>');
    }
    else {
      if (self::$useHtml) {
        $log = str_replace("\n", "<br>", $log);
      }
      echo '[log] ' . $log;
    }
    flush();
  }

  /**
   * Log to nothing, ignore all messages.
   */
  public static function blackHole() {}

}
